<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Nilai PHP</title>
	<link rel="stylesheet" href="">
</head>
<body>

<h3>Memberikan Nilai Berdasarkan angka</h3>

<?php

function tentukan_nilai($number)
{
	if ($number) {
		echo $number .' - <b>Sangat Baik</b> <br><br>';
	
	} elseif ($number) {
		echo $number .' - <b>Baik</b> <br><br>';
		
	}elseif ($number) {
		echo $number .' - <b>Cukup</b> <br><br>';
	
	}elseif ($number) {
		echo $number .' - <b>Kurang</b>';
	}
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>
	
</body>
</html>