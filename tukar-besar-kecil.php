<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
</head>
<body>

<?php 

function tukar_besar_kecil($string){
	for ($i = 0; $i < strlen($string) ; $i++) {
		if (ctype_upper($string[$i])) {
			$new_string[$i] = strtolower($string[$i]);
		}
		else {
			$new_string[$i] = strtoupper($string[$i]);
		}
	}

	return implode($new_string) . '<br>';
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

 ?>
	
</body>
</html>