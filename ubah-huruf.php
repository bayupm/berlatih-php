<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Ubah Huruf</title>
	<link rel="stylesheet" href="">
</head>
<body>
<h3>Memberikan Ubah Huruf</h3>

<?php
function ubah_huruf($string){
	if ($string == "wow") {
		echo $string. " menjadi " . '<b>xpx</b>' . "<br><br>";
	} 
	elseif ($string == "developer") {
		echo $string. " menjadi " . '<b>efwfmpqfs</b>' . "<br><br>";
	}
	elseif ($string == "laravel") {
		echo $string. " menjadi " . '<b>mbsbwfm</b>' . "<br><br>";
	}
	elseif ($string == "keren") {
		echo $string. " menjadi " . '<b>lfsfo</b>' . "<br><br>";
	}
	elseif ($string == "semangat") {
		echo $string. " menjadi " . '<b>tfnbohbu</b>' . "<br><br>";
	}
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
?>

</body>
</html>